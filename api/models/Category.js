module.exports = {

  connection: 'categoriesDb',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    tag: {
      type: 'string',
      size: 30,
      unique: true,
      required: true
    }
  }
  
}
