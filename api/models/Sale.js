module.exports = {

  connection: 'salesDb',

  attributes: {
    id: {
      type: 'integer',
      autoIncrement: true,
      primaryKey: true,
      unique: true
    },
    date: {
      type: 'string',
      defaultsTo: new Date().toISOString().split('T')[0]
    },
    time: {
      type: 'string',
      defaultsTo: new Date().toISOString().split('T')[1].split('.')[0]
    },
    products: {
      type: 'json',
      defaultsTo: {}
    }
  }

}
