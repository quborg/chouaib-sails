'use strinct'

module.exports = {
  // ALL action
  index: function(req, res, next) {
    Product.find(req.query, function(err, products) {
      if (products === undefined) return res.notFound();
      if (err) return next(err);
      products.forEach(function(p,i){
        Category.findOne(products[i].category, function(err, category) {
          products[i].category = category === undefined || err ? {id:0, tag:'الكل'} : {id:category.id, tag:category.tag}
          products.length == i+1 ? res.json(products) : null
        })
      })
    });
  },
  // CREATE action  
  create: function(req, res, next) {
    var params = req.params.all();
    Product.create(params, function(err, product) {
      if (err) return next(err);
      res.status(201);
      res.json(product);
    });
  },
  // a FIND action
  find: function(req, res, next) {
    var id = req.param('id');
    Product.findOne(id, function(err, product) {
      if (product === undefined) return res.notFound();
      if (err) return next(err);
      res.json(product);
    });
  },
  // an UPDATE action
  update: function(req, res, next) {
    var criteria = {};
    criteria = _.merge({}, req.params.all(), req.body);
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Product.update(id, criteria, function(err, product) {
      if (product === undefined) return res.notFound();
      if (err) return next(err);
      res.json(product);
    });
  },
  // a DESTROY action
  destroy: function(req, res, next) {
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Product.findOne(id).exec(function(err, result) {
      if (err) return res.serverError(err);
      if (!result) return res.notFound();
      Product.destroy(id, function(err) {
        if (err) return next(err);
        return res.json(result);
      });
    });
  },
}
