module.exports = {
  // ALL action
  index: function(req, res, next) {
    Category.find(req.query, function(err, categories) {
      if (categories === undefined) return res.notFound();
      if (err) return next(err);
      res.json(categories);
    });
  },
  // CREATE action  
  create: function(req, res, next) {
    var params = req.params.all();
    Category.create(params, function(err, category) {
      if (err) return next(err);
      res.status(201);
      res.json(category);
    });
  },
  // a FIND action
  find: function(req, res, next) {
    var id = req.param('id');
    Category.findOne(id, function(err, category) {
      if (category === undefined) return res.notFound();
      if (err) return next(err);
      res.json(category);
    });
  },
  // an UPDATE action
  update: function(req, res, next) {
    var criteria = {};
    criteria = _.merge({}, req.params.all(), req.body);
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Category.update(id, criteria, function(err, category) {
      if (category === undefined) return res.notFound();
      if (err) return next(err);
      res.json(category);
    });
  },
  // a DESTROY action
  destroy: function(req, res, next) {
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Category.findOne(id).exec(function(err, result) {
      if (err) return res.serverError(err);
      if (!result) return res.notFound();
      Category.destroy(id, function(err) {
        if (err) return next(err);
        return res.json(result);
      });
    });
  },
}
