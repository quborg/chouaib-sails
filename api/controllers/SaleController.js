'use strinct'

module.exports = {
  // ALL action
  index: function(req, res, next) {
    Sale.find(req.query,function(err, sales) {
      if (sales === undefined) return res.notFound()
      if (err) return next(err)
      res.json(sales)
    })
  },
  // CREATE action
  create: function(req, res, next) {
    var params = req.params.all();
    console.log(req.query)
    Sale.create(params, function(err, sale) {
      if (err) return next(err);
      res.status(201);
      res.json(sale);
    });
  },
  // a FIND action
  find: function(req, res, next) {
    var id = req.param('id');
    Sale.findOne(id, function(err, sale) {
      if (sale === undefined) return res.notFound();
      if (err) return next(err);
      res.json(sale);
    });
  },
  // an UPDATE action
  update: function(req, res, next) {
    var criteria = {};
    criteria = _.merge({}, req.params.all(), req.body);
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Sale.update(id, criteria, function(err, sale) {
      if (sale === undefined) return res.notFound();
      if (err) return next(err);
      res.json(sale);
    });
  },
  // a DESTROY action
  destroy: function(req, res, next) {
    var id = req.param('id');
    if (!id) {
      return res.badRequest('No id provided.');
    }
    Sale.findOne(id).exec(function(err, result) {
      if (err) return res.serverError(err);
      if (!result) return res.notFound();
      Sale.destroy(id, function(err) {
        if (err) return next(err);
        return res.json(result);
      });
    });
  },
}
