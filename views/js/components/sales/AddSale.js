'use strict'

var $ = require('jquery'),
    React = require('react'),
    SaleFields = require('./SaleFields'),
    Site = require('../../site/settings'),
    Modal = require('../../templates/Modal'),
    Helper = require('../../helpers/Helper'),
    BtnGly = require('../../templates/BtnGly'),
    Services = require('../../services/Sales')
import { Button } from 'react-bootstrap'

module.exports = React.createClass({
  getInitialState: function() { return { showModal: false, }},
  open: function() { this.setState({showModal:true}) },
  close:function() { this.setState({showModal:false}) },
  save: function() {
    if(Helper.ValidateSale('add-sale')) {
      let form = $('#add-sale form').serialize().replace(/%5B/g, '[').replace(/%5D/g, ']')
      Services.saveSale(form)
      this.setState({ showModal: false })
    }
  },
  render: function() {
    let title = Site.Views.Sales.addModal.title,
        save = (<Button onClick={this.save} bsStyle="primary">حفظ</Button>),
        cancel = (<Button onClick={this.close}>إلغاء</Button>),
        body = (<SaleFields/>)
    return (
      <div>
        <BtnGly Style="primary" Click={this.open} Gly="download-alt" aftG=" جديد"/>
        <Modal id="add-sale" show={this.state.showModal} onHide={this.close} title={title} submit={save} cancel={cancel} body={body}/>
      </div>
    )
  }
})
