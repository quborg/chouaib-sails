'use strict'

var $ = require('jquery'),
    React = require('react'),
    Assign = require('object-assign'),
    Mixins = require('../../mixins/Mixins'),
    Services = require('../../services/Products'),
    Typeahead = require('react-typeahead').Typeahead
import { Table, Glyphicon } from 'react-bootstrap'

function getProducts() { 
  return { 
    products : Services.get_products().map( (p,i) => {
      return {
        id:p.id, 
        title:p.title
      }
    })
  } 
}

function getTotals() {
  let totals = 0
  $('input[id^="tot-"]').map((i,v)=>{ totals += Number(v.value) })
  return totals
}

Services.getProducts()

module.exports = React.createClass({
  mixins: [Mixins(getProducts)],
  getInitialState: function() { 
    return {
      key: 0,
      rows: [],
      total: 0
    }
  },
  displayOption: function(option) { return option.title },
  optionSelected: function(i,option) {
    let rows = Assign([],this.state.rows)
    rows[i].id = option.id
    rows[i].title = option.title
    this.setState({rows:rows})    
  },
  multiply: function(i, l, e) {
    let qty = document.getElementById('qty-'+i).value || 0,
        prx = document.getElementById('prx-'+i).value || 0,
        values = Assign({},this.state.values),
        result = Number(qty) * Number(prx),
        rows = Assign([],this.state.rows)
    rows[i].quantity = qty
    rows[i].price = prx
    this.setState({rows:rows})
    document.getElementById('tot-'+i).value = result.toFixed(2)
  },
  setTotal: function() { this.setState({total:getTotals()}) },
  remove: function(i) {
    let rows = Assign([],this.state.rows)
    console.log('remove -> ',T)
    this.setState({rows:rows}, function() {
      this.setState({total:getTotals()})
    }.bind(this))
  },
  addRow: function(entity) {
    let k = this.state.key,
        rows = Assign([],this.state.rows),
        e = entity == undefined ? {id:[''],title:[''],quantity:[''],price:['']} : entity
    e.id.map((v,i)=>{
      rows[k++] = {id:e.id[i],title:e.title[i],quantity:e.quantity[i],price:e.price[i]}
    })
    this.setState({key:k,rows:rows})
  },
  loopProducts: function(p) {
    this.addRow(p)
  },
  componentWillMount: function() {
    this.props.data !== undefined ? this.loopProducts(this.props.data.products) : this.addRow()
  },
  render: function() {
    let columns = ['', 'الاسم', 'العدد', 'الثمن', 'المجموع'],
        classes = "table-ticket table-bordered table-condensed table-hover"
    return (
      <form className="form-horizontal">
        <h5>المنتوجات :</h5>
        <Table className={classes}>
          <thead><tr>{columns.map((c,i)=>{return<th key={i}>{c}</th>})}</tr></thead>
          <tbody id="sale-items">
            {this.state.rows.map( (r,i) => {
              return (
                <tr key={i} id={'row-'+i}>
                  <td><Glyphicon onClick={this.remove.bind(null,i)} glyph="remove-circle" className="btn-ticket del"/></td>
                  <td><input type="hidden" id={'pid-'+i} name="products[id][]" value={r.id} />
                    <Typeahead key={i} ref={"title-"+i}
                      filterOption='title' 
                      name="products[title][]" 
                      options={this.state.products} 
                      value={r.title}
                      displayOption={this.displayOption}
                      onOptionSelected={this.optionSelected.bind(this,i)} />
                  </td>
                  <td><input type="number" name="products[quantity][]" id={'qty-'+i} onChange={this.multiply.bind(null,i,'qty')} value={r.quantity} /></td>
                  <td><input type="number" name="products[price][]" id={'prx-'+i} onChange={this.multiply.bind(null,i,'prx')} step="0.01" value={r.price} /></td>
                  <td><input type="number" id={'tot-'+i} disabled/></td>
                </tr>
              )
            })}
          </tbody>
          <tfoot>
            <tr><td className="w-50"><Glyphicon onClick={this.addRow.bind(null,undefined)} glyph="circle-arrow-left" className="btn-ticket add"/></td>
                <td className="ticket-txt"></td><td className="ticket-num"></td><td className="ticket-num"></td><td className="ticket-tot"><input type="number" value={this.state.total} disabled/></td></tr>
          </tfoot>
        </Table>
      </form>
    )
  }
})
