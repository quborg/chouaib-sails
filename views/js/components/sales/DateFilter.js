'use strict'

var React = require('react'),
    Services = require('../../services/Sales'),
    DatePicker = require('../../templates/DatePicker')

module.exports = React.createClass({
  getInitialState: function() { return { date: '' } },
  dateFilter: function(date) {
    this.setState({date: date})
    let query = '?date=' + date._d.toISOString().split('T')[0]
    Services.getSales(query)
  },
  render: function() { return ( <DatePicker date={this.state.date} change={this.dateFilter}/> ) }
})
