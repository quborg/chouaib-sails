'use strict'

var React = require('react'),
    Assign = require('object-assign'),
    Site = require('../../site/settings'),
    CategoriesSelect = require('./CategoriesSelect')
import { Input } from 'react-bootstrap'

module.exports = React.createClass({
  getInitialState: function() {
    let data = this.props.data
    return {
      values: {
        title:          data ? this.props.data.title          : '',
        sale_price:     data ? this.props.data.sale_price     : '',
        purchase_price: data ? this.props.data.purchase_price : '',
        quantity:       data ? this.props.data.quantity       : '',
        category:       data ? this.props.data.category.id    : ''
      }
    }
  },
  onChange: function(name, e) {
    let Value = e.target.value,
        Values = Assign({},this.state.values)
    Values[name] = Value
    this.setState({ values: Values })
  },
  render: function() {
    return (
      <form className="form-horizontal">
        {Site.Views.Products.fields.map((s,i)=>{
          return(
          <Input 
            key={i} 
            min={s.min} 
            ref={s.name} 
            name={s.name} 
            type={s.type} 
            step={s.step}
            label={s.title} 
            labelClassName="col-xs-3" 
            wrapperClassName="col-xs-9" 
            value={this.state.values[s.name]}
            onChange={this.onChange.bind(null, s.name)}/>
        )})}
        <CategoriesSelect 
          id="add-cat2prod" 
          name="category" 
          label="الصنف" 
          labelCN="col-xs-3" 
          wrapperCN="col-xs-9" 
          onChange={this.onChange.bind(null, 'category')}
          value={this.state.values.category}/>
      </form>
    )
  }
})