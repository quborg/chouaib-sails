'use strict'

var React = require('react'),
    Add = require('./AddProduct'),
    Filter = require('./CategoriesFilter')
import { ButtonToolbar, Col } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <ButtonToolbar className="tool-bar">
        <Col xs={12} md={8}><Add/></Col>
        <Col xs={6} md={4}><Filter/></Col>
      </ButtonToolbar>
    )
  }
})
