'use strict'

var React = require('react'),
    Mixins = require('../../mixins/Mixins'),
    BtnGly = require('../../templates/BtnGly'),
    Services = require('../../services/Products'),
    CategoriesSelect = require('./CategoriesSelect'),
    CategoriesManager = require('./CategoriesManager')

module.exports = React.createClass({
  getInitialState: function() {
    return { showModal: false } 
  },
  open: function() { this.setState({showModal:true}) },
  close: function() { this.setState({showModal:false}) },
  filterCategory: function(e) {
    let id = e.target.value,
        query = id == '1' ? '' : '?category=' + e.target.value
    Services.getProducts(query)
  },
  render: function() {
    let man = (<BtnGly Style="primary" Click={this.open} Gly="cog"/>)
    return (
      <div>
        <CategoriesSelect 
          btnBefore={man} 
          id="filter-prodsBycat" 
          name="categories-list"
          onChange={this.filterCategory}/>
        <CategoriesManager 
          close={this.close} 
          id="categories-manager"
          show={this.state.showModal}/>
      </div>
    )
  }
})
