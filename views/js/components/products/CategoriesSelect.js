'use strict'

var React = require('react'),
    Mixins = require('../../mixins/Mixins'),
    Service = require('../../services/Categories')
import { Input } from 'react-bootstrap'

Service.getCategories()

function getCategories() { return { categories: Service.get_categories() } }

module.exports = React.createClass({
  mixins: [Mixins(getCategories)],
  render: function() {
    return (
      <Input 
        type="select" 
        id={this.props.id} 
        name={this.props.name} 
        label={this.props.label} 
        value={this.props.value} 
        onChange={this.props.onChange} 
        buttonAfter={this.props.btnAfter} 
        buttonBefore={this.props.btnBefore} 
        labelClassName={this.props.labelCN} 
        wrapperClassName={this.props.wrapperCN}>
        {this.state.categories.map((o,i)=>{return <option key={i} value={o.id}>{o.tag}</option>})}
      </Input>
    )
  }
})
