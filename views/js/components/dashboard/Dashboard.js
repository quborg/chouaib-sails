'use strict'

var React = require('react'),
    Mixins = require('../../mixins/Mixins')

function getBlocks() {
  return { 
    blocks: [
      {"name": "Sales Revision"}
    ]
  }
}

module.exports = React.createClass({
  mixins: [Mixins(getBlocks)],
  render: function() {
    return (
      <div className="row">{this.state.blocks.map((bloc)=>{return <div key={bloc.name} item={bloc}>{bloc.name}</div>})}
      </div>
    )
  }
})
