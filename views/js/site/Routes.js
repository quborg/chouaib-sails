'use strict'

import { Router, Route, IndexRoute } from 'react-router'
import createBrowserHistory from 'history/lib/createBrowserHistory'

var React = require('react'),
    Template = require('../templates/Template'),
    Sales = require('../components/sales/Sales'),
    NotFoundPage = require('../templates/NotFoundPage'),
    Products = require('../components/products/Products'),
    Invoices = require('../components/invoices/Invoices'),
    Dashboard = require('../components/dashboard/Dashboard'),
    history = createBrowserHistory()

module.exports = (
  <Router history={history}>
    <Route path="/" component={Template}>
      <IndexRoute component={Dashboard}/>
      <Route path="/sales" component={Sales}/>
      <Route path="/products" component={Products}/>
      <Route path="/invoices" component={Invoices}/>
      <Route path="*" component={NotFoundPage}/>
    </Route>
  </Router>
)
