'use strict'

var $ = require('jquery'),
    React = require('react'),
    Toastr = require('toastr')

function printError() { 
  Toastr["error"]('المرجو تصحيح الاخطاء !')
  return 0
}

module.exports = {

  ValidateSale: function(id) {
    let err = 0,
        inputs = $('#'+id+' form input[name="products[title][]"]')
    inputs.each(function(k,v){
      v.nextSibling.className = ''
      if(v.value=='') {
        v.nextSibling.className = 'has-error'
        err = 1
      }
    })
    console.log('error : '+err)
    return err ? printError() : 1
  },

  ValidateProduct: function(id) {
    let err = 0,
        inputs = $('#'+id+' form input')
    inputs.each(function(k,v){
      v.parentElement.parentElement.className = "form-group"
      if(v.value=='') {
        v.parentElement.parentElement.className = "form-group has-error"
        err = 1
      }
    })
    return err ? printError() : 1
  }

}
