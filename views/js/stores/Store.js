var assign = require('object-assign'),
    Sales = require('../services/Sales'),
    Products = require('../services/Products'),
    Constants = require('../constants/Constants'),
    EventEmitter = require('events').EventEmitter,
    Categories = require('../services/Categories'),
    Dispatcher = require('../dispatchers/Dispatcher'),
    CHANGE_EVENT = 'change'

var AppStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT)
  },
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback)
  },
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback)
  },
  getSales: function() {
    return Sales.get_sales()
  },
  getProducts: function() {
    return Products.get_products()
  },
  getCategories: function() {
    return Categories.get_categories()
  }
})

dispatcherIndex: Dispatcher.register(function(payload) {
  var action = payload.action
  switch (action.actionType){
    case Constants.GET_SALES:
      Sales.set_sales(action.sales)
      break
    case Constants.GET_PRODUCTS:
      Products.set_products(action.products)
      break
    case Constants.GET_CATEGORIES:
      Categories.set_categories(action.categories)
      break
  }
  AppStore.emitChange()
  return true
})

module.exports = AppStore
