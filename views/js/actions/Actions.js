var Constants = require('../constants/Constants'),
    Dispatcher = require('../dispatchers/Dispatcher')

module.exports = {
  getSales:function(sales) {
    Dispatcher.handleServerAction({
      actionType: Constants.GET_SALES,
      sales: sales
    })
  },
  getProducts:function(products) {
    Dispatcher.handleServerAction({
      actionType: Constants.GET_PRODUCTS,
      products: products
    })
  },
  getCategories:function(categories) {
    Dispatcher.handleServerAction({
      actionType: Constants.GET_CATEGORIES,
      categories: categories
    })
  }
}
