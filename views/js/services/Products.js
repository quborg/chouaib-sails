'use strinct'

var $ = require('jquery'),
    Toastr = require('toastr'),
    Actions = require('../actions/Actions'),
    _products = []

Toastr.options = {
  "progressBar": true,
  "positionClass": "toast-top-left",
  "timeOut": "5000"
}

function getProducts(q) { var q = q || ''
  $.ajax({
    url: 'http://192.168.1.74:3000/api/products'+q,
    success: Actions.getProducts,
    error: Actions.getProducts(_products)
  })
}

module.exports = {

  get_products: function() { return _products },

  set_products: function(p) { if(p) _products = p },

  getProducts: function(q) { getProducts(q) },

  saveProduct: function(form) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/product',
      type: 'POST',
      data: form,
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ التسجيل بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو الإعادة !')
        getProducts()
      }
    })
  },

  removeProduct: function(id) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/product/'+id,
      type: 'DELETE',
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ إزالة المنتوج بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
        getProducts()
      }
    })
  },

  updateProduct: function(id,p) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/product/'+id,
      type: 'PUT',
      data: p,
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ حفظ التحرير بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
        getProducts()
      }
    })
  }

}
