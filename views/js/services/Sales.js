'use strinct'

var $ = require('jquery'),
    Toastr = require('toastr'),
    Actions = require('../actions/Actions'),
    _sales = []

Toastr.options = {
  "progressBar": true,
  "positionClass": "toast-top-left",
  "timeOut": "5000"
}

function getSales(q) { var q = q || '?date=' + new Date().toISOString().split('T')[0]
  $.ajax({
    url: 'http://192.168.1.74:3000/api/sales'+q,
    success: Actions.getSales,
    error: Actions.getSales
  })
}

module.exports = {

  get_sales: function() { return _sales },

  set_sales: function(s) { if(s) _sales = s },

  getSales: function(q) { getSales(q) },

  getSale: function(id) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/sale',
      type: 'POST',
      data: form,
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ التسجيل بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو الإعادة !')
        getSales()
      }
    })
  },

  saveSale: function(form) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/sale',
      type: 'POST',
      data: form,
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ التسجيل بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو الإعادة !')
        getSales()
      }
    })
  },

  removeSale: function(id) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/sale/'+id,
      type: 'DELETE',
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ إزالة المنتوج بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
        getSales()
      }
    })
  },

  updateSale: function(id,s) { let flag = 0
    $.ajax({
      url: 'http://192.168.1.74:3000/api/sale/'+id,
      type: 'PUT',
      data: s,
      success: function() { flag = 'stu' },
      error: function() { flag = 'err' },
      complete: function() {
        if(flag=='stu') Toastr["success"]('لقد تمّ حفظ التحرير بنجاح !')
        if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
        getSales()
      }
    })
  }

}
