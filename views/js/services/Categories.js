'use strinct'

var $ = require('jquery'),
    Toastr = require('toastr'),
    Actions = require('../actions/Actions'),
    _categories = [{id:1,tag:'الكل'}]

module.exports = {

  get_categories: function() { return _categories },

  set_categories: function(c) { if(c) _categories = c },

  getCategories: function() {
    $.ajax({
      url: 'http://192.168.1.74:3000/api/categories',
      success: Actions.getCategories,
      error: function() { Toastr["error"]('وقع خطأ, المرجو إعادة تحميل الصفحة !') }
    })
  },

  saveCategories: function(cats) {
    let err = 0
    cats.map((c,i)=>{
      $.ajax({
        url: 'http://192.168.1.74:3000/api/category',
        type: 'POST',
        data: "tag="+c.tag,
        error: function() { err = 1 },
        complete: function() {
          if(cats.length==++i) {
            !err ? Toastr["success"]('لقد تمّ التسجيل بنجاح !') : Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
            --window.ajaxFlag
          }
        }
      })
    })
  },

  removeCategories: function(cats) {
    let flag = 0
    cats.map((id,i)=>{
      $.ajax({
        url: 'http://192.168.1.74:3000/api/category/'+id,
        type: 'DELETE',
        success: function() { flag = 'stu' },
        error: function(xhr, txt, err) { flag = 'err' },
        complete: function() {
          if(cats.length==++i) {
            if(flag=='stu') Toastr["success"]('لقد تمّ الحدف بنجاح !')
            if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
            --window.ajaxFlag
          }
        }
      })
    })
  },

  updateCategories: function(cats) {
    let flag = 0
    cats.map((c,i)=>{
      $.ajax({
        url: 'http://192.168.1.74:3000/api/category/'+c.id,
        type: 'PUT',
        data: "tag="+c.tag,
        success: function() { flag = 'stu' },
        error: function(xhr, txt, err) { flag = 'err' },
        complete: function() {
          if(cats.length==++i) {
            if(flag=='stu') Toastr["success"]('لقد تمّ التحديث بنجاح !')
            if(flag=='err') Toastr["error"]('وقع خطأ, المرجو إعادة العملية !')
            --window.ajaxFlag
          }
        }
      })
    })
  }

}
