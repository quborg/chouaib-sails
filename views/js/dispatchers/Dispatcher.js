var Dispatcher = require('flux').Dispatcher,
    assign = require('object-assign')

module.exports = assign(new Dispatcher(), {

  handleServerAction: function(action) {
    let payload = {
      source: 'SERVER_ACTION',
      action: action
    }
    this.dispatch(payload)
  }

})
