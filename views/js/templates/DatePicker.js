'use strict'

var React = require('react'),
    moment = require('moment'),
    DatePicker = require('react-datepicker'),
    Services = require('../services/Sales')

module.exports = React.createClass({
  getInitialState: function() { return { date: moment() } },
  changeHandler: function(date) { this.setState({date: date}) },
  render: function() {
    return (
      <DatePicker 
        name={this.props.name}
        weekdays={['ح','ن','ث','ر','خ','ج','س']} 
        onChange={this.props.change||this.changeHandler} 
        selected={this.props.date||this.state.date} />
    )
  }
})
