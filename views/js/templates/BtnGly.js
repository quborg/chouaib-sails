'use strict'

var React = require('react')
import { Button, Glyphicon } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    let Btn = this.props
    return (
      <Button 
        className={Btn.Class}
        bsStyle={Btn.Style} 
        bsSize={Btn.Size}
        onClick={Btn.Click}>
        {Btn.befG} 
        <Glyphicon glyph={Btn.Gly}/> 
        {Btn.aftG}
      </Button>
    )
  }
})
