'use strict'

var React = require('react')
import { Modal, Button } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    let M = this.props
    return (
      <Modal id={M.id} show={M.show} onHide={M.onHide}>
        <Modal.Header> <Modal.Title> {M.title} </Modal.Title> </Modal.Header>
        { !!M.body ? (<Modal.Body> {M.body} </Modal.Body>) : '' }
        <Modal.Footer> {M.submit} {M.cancel} </Modal.Footer>
      </Modal>
    )
  }
})