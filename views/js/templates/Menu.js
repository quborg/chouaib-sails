'use strict'

var React = require('react'),
    Link = require('react-router').Link,
    Settings = require('../site/settings')
import { Navbar, NavBrand, Nav, Glyphicon } from 'react-bootstrap'
import { IndexLink } from 'react-router'

module.exports = React.createClass({
  render: function() {
    return (
      <Navbar fixedTop fluid>
        <NavBrand>
          <IndexLink to="/" activeClassName="active"><Glyphicon glyph="stats"/></IndexLink>
        </NavBrand>
        <Nav>
          {Settings.mainMenu.map((l,k)=>(<li key={k}><Link to={l.to} activeClassName="active">{l.title}</Link></li>))}
        </Nav>
      </Navbar>
    )
  }
})
