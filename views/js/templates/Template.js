'use strict'

var React = require('react'),
    Menu = require('./Menu')
import { Grid } from 'react-bootstrap'

module.exports = React.createClass({
  render: function() {
    return (
      <Grid fluid>
        <Menu/>
        <div className="container-fluid">
          {this.props.children}
        </div>
      </Grid>
    )
  }
})
