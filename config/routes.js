module.exports.routes = {

  // SALES ROUTES
  'GET     /api/sales?'     : 'Sale.index',
  'POST    /api/sale'      : 'Sale.create',
  'GET     /api/sale/:id'  : 'Sale.find',
  'PUT     /api/sale/:id?' : 'Sale.update',
  'DELETE  /api/sale/:id?' : 'Sale.destroy',

  // PRODUCTS ROUTES
  'GET     /api/products'     : 'Product.index',
  'POST    /api/product'      : 'Product.create',
  'GET     /api/product/:id'  : 'Product.find',
  'PUT     /api/product/:id?' : 'Product.update',
  'DELETE  /api/product/:id?' : 'Product.destroy',

  // CATEGORIES ROUTES
  'GET    /api/categories'    : 'Category.index',
  'POST   /api/category'      : 'Category.create',
  'GET    /api/category/:id'  : 'Category.find',
  'PUT    /api/category/:id?' : 'Category.update',
  'DELETE /api/category/:id?' : 'Category.destroy',

}
