module.exports = {

  port: 3000,

  connections: {
    salesDb: {
      adapter: 'sails-disk',
      filePath: 'data/',
      fileName: 'sales.db'
    },
    productsDb: {
      adapter: 'sails-disk',
      filePath: 'data/',
      fileName: 'products.db'
    },
    categoriesDb: {
      adapter: 'sails-disk',
      filePath: 'data/',
      fileName: 'categories.db'
    },
    invoicesDb: {
      adapter: 'sails-disk',
      filePath: 'data/',
      fileName: 'invoices.db'
    }
  },

  models: {
    migrate: 'alter',
    schema: true,
    autoPK: false
  },

  blueprints: {
    actions: false,
    rest: false,
    shortcuts: false,
    // prefix: '',
    // restPrefix: '',
    pluralize: false,
    // populate: true,
    // autoWatch: true,
    // defaultLimit: 30
    index: false
  },

  // i18n: {
  //   locales: ['ar', 'en'],
  //   defaultLocale: 'en',
  //   updateFiles: false,
  //   localesDirectory: '/config/locales'
  // },

  // csrf: {
  //   grantTokenViaAjax: true,
  //   origin: ''
  // },

  cors: {
    allRoutes: true,
    origin: '*',
    credentials: true,
    methods: 'GET, POST, PUT, DELETE', //, OPTIONS, HEAD
    // headers: 'content-type'
  },

  // globals: {
  //   _: true,
  //   async: true,
  //   sails: true,
  //   services: true,
  //   models: true
  // },

  // log: {
  //   level: 'info'
  // },

  // policies: {
  //   '*': true,
  //   RabbitController: {
  //     '*': false,
  //     nurture : 'isRabbitMother',
  //     feed : ['isNiceToAnimals', 'hasRabbitFood']
  //   }
  // },

  autoreload: {
    active: true,
    usePolling: false,
    dirs: [
      "api/models",
      "api/controllers"
    ]
  }

}
